<div id="header">
  <p align="center"><img src="https://media.giphy.com/media/M9gbBd9nbDrOTu1Mqx/giphy.gif" width="100"/></p>
  <p align="center"><img src="https://img.shields.io/gitlab/stars/44095126?style=for-the-badge"></p>
</div>

---

### &nbsp;About Me :

I'm just a guy who likes programming... <img src="https://media.giphy.com/media/WUlplcMpOCEmTGBtBW/giphy.gif" width="30"> from Italy.

- 🔭 i am studying at the university.
- 🌱 I’m currently learning **C/C++**
- ⚡ In my free time I sleep or play videogames.
- 💬 Fun fact **I hate OOP**

---

### &nbsp;Social :
<p align="left">
  <div id="badges">
    <a href="https://twitter.com/Cikuozzo">
      <img src="https://img.shields.io/badge/Twitter-blue?style=for-the-badge&logo=twitter&logoColor=white" alt="Twitter Badge" height="30" height="120"/>
    </a>
    <a href="https://www.reddit.com/user/Cikuozzo">
      <img src="https://img.shields.io/badge/Reddit-FF4500?style=for-the-badge&logo=reddit&logoColor=white" alt="Reddit Badge" height="30" height="120"/>
    </a>
    <a href="https://www.facebook.com/profile.php?id=100029266174858">
      <img src="https://img.shields.io/badge/Facebook-1877F2?style=for-the-badge&logo=facebook&logoColor=white" alt="Facebook Badge" height="30" height="120"/>
    </a>
    <a href="https://www.instagram.com/roccorotondo_/">
      <img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white" alt="instagram Badge" height="30" height="120"/>
    </a>
  </div>  
</p>

---

### &nbsp;Languages and tools :
<p align="left"> 
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> 
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a> 
<img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> 
<img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/gitlab/gitlab-original.svg" alt="gitlab" width="40" height="40"/> </a>
<img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/vim/vim-plain.svg" alt="vim" width="40" height="40"/></a>
</p>

---

<div align="center">
    <img src="https://forthebadge.com/images/badges/built-with-love.svg" />
    <img src="https://forthebadge.com/images/badges/built-by-developers.svg" />
    <img src="https://forthebadge.com/images/badges/0-percent-optimized.svg" />
    <img src="https://forthebadge.com/images/badges/contains-tasty-spaghetti-code.svg" />
    <img src="https://forthebadge.com/images/badges/open-source.svg">
    <img src="https://forthebadge.com/images/badges/not-a-bug-a-feature.svg">
    <img src="https://forthebadge.com/images/badges/powered-by-black-magic.svg" />
</div>

